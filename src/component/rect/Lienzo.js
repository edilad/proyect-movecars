import React, { Component } from 'react';
import {Rect} from 'react-konva';

class Lienzo extends Component{

    constructor(...args) {
        super(...args);
        this.state = {
          color: 'gray'
        };       
      }     
      render() {
          return (
              <Rect
                  x={270} y={20} width={850} height={500}
                  fill={this.state.color}
                  shadowBlur={10}
                  onClick={this.handleClick}
              />
          );
      }
  }

  export default Lienzo
