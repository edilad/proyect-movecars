import React, { Component } from 'react';
import {Stage, Layer} from 'react-konva';
import './App.css';
import Lienzo from '../../component/rect/Lienzo';
import Figura from '../../component/figura/Figura';

class App extends Component {
    constructor(props){
      super(props)
      this.state = {} 
    }

  handleClick =() =>{
    console.log('this is:', this);
  }

  render() {
    return (
      <form>      
        <Stage width={3000} height={2000}>        
        <Layer>          
          <Lienzo/> 
          <Figura/>        
        </Layer>           
        </Stage>
        <div className="form-group">
        <label htmlFor="avanzar" type="text">Desplazamiento</label>
        <br/>
        <input name="avanzar" type="text" required/>
        </div>

        <div className="form-group-direccion">
        <label htmlFor="direccion" type="text">Direccion</label>
        <br/>
        <input name="direccion" type="text" required/>
        </div>
        <button  onClick={this.handleClick} type="button" class="btn">
          Mover
        </button>  
      </form>
    );
  }
}

export default App;
