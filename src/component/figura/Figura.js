import React, { Component } from 'react';
import {Circle} from 'react-konva';

class Figura extends Component{
    constructor(...args) {
        super(...args);
        this.state = {
           isMouseInside: false
        };
        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.desplazar = this.desplazar.bind(this);
      }
      handleMouseEnter() {
        this.setState({
          isMouseInside: true
        });
      }
      handleMouseLeave() {
        this.setState({
          isMouseInside: false
        });
      }

      desplazar =(objeto) =>{
        let tecla = objeto.which;
        let y = document.getElementById("Circle").offsetLeft;
        // let x = document.getElementById("Circle").offsetTop;
        switch(tecla){
          case 37:
            Circle.left = y -220+"px"; break;
            default: alert("Se ha equivocado, debe pulsar las flechas del teclado");
        }

      }
    
    render() {
        return (
            <Circle
                x={350} y={80} radius={40}
                fill="yellow"
                stroke="black"
                strokeWidth={this.state.isMouseInside ? 5 : 1}  
                desplazar={this.desplazar}         
            />
        );
    }

}

export default Figura